/* -*- c++ -*- */
/* 
 * Copyright 2016 mikuslaw.
 * 
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 * 
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <gnuradio/io_signature.h>
#include "clock_recovery_zero_ff_impl.h"
#include <iostream>

namespace gr {
  namespace keeloq {

    clock_recovery_zero_ff::sptr
    clock_recovery_zero_ff::make(int samplesPerSymbol, int holdSamplingForNrOfSymbols)
    {
      return gnuradio::get_initial_sptr
        (new clock_recovery_zero_ff_impl(samplesPerSymbol, holdSamplingForNrOfSymbols));
    }

    /*
     * The private constructor
     */
    clock_recovery_zero_ff_impl::clock_recovery_zero_ff_impl(int samplesPerSymbol, int holdSamplingForNrOfSymbols)
      : gr::block("clock_recovery_zero_ff",
              gr::io_signature::make(1, 1, sizeof(float)),
              gr::io_signature::make(1, 1, sizeof(float))),
              m_SignalPositive(0),              // Previous signal state.
              m_SignalValueUnity(0),            // Current signal state.
              m_SampleForNrOfSymbols(0),        // How much more symbols do we expect.
              m_SampleForNrOfSymbolsMax(holdSamplingForNrOfSymbols),    // Maxiumum number of symbols that we expect to see without sign change.
              m_SamplesTillSymbolMidpoint(0),   // How many samples are till next symbol midpoint.
              m_SamplesPerSymbol(samplesPerSymbol)            // Number of samples per symbol.
    {}

    /*
     * Our virtual destructor.
     */
    clock_recovery_zero_ff_impl::~clock_recovery_zero_ff_impl()
    {
    }

    void
    clock_recovery_zero_ff_impl::forecast (int noutput_items, gr_vector_int &ninput_items_required)
    {
      ninput_items_required[0] = noutput_items;
    }

    int
    clock_recovery_zero_ff_impl::general_work (int noutput_items,
                       gr_vector_int &ninput_items,
                       gr_vector_const_void_star &input_items,
                       gr_vector_void_star &output_items)
    {
      const float *in = (const float *) input_items[0];
      float *out = (float *) output_items[0];

      int samplesLeft = ninput_items[0];    // Input samples left in the buffer.
      int currentOutputSample = 0;          // Output samples produced.
      
      // For the first sample we don't know if the "previous" signal 
      // was possitive or negative, so just sample the input.
      if( m_SignalPositive == 0)
      {
          if( in[0] >= 0 )
          {
              m_SignalPositive = 1;
          }
          else
          {
              m_SignalPositive = -1;
          }
      }
      
      while( samplesLeft > 0 && currentOutputSample < noutput_items )
      {
          // Find the sign of current signal.
          if( (in[ninput_items[0] - samplesLeft]) >= 0 )
          {
              m_SignalValueUnity = 1;
          }
          else
          {
              m_SignalValueUnity = -1;
          }
          
          // Check if the sign changed in the signal.
          if( m_SignalValueUnity != m_SignalPositive )
          {
              // Sign changed. Enable sampling for next 
              // m_SampleForNrOfSymbolsMax symbols. Try to catch 
              // a midpoint of a given symbol.
              m_SampleForNrOfSymbols = m_SampleForNrOfSymbolsMax;
              m_SamplesTillSymbolMidpoint = m_SamplesPerSymbol/2U;
              m_SignalPositive = m_SignalValueUnity;
          }
          
          // Are we sampling the signal?
          if( m_SampleForNrOfSymbols > 0 )
          {
              // Yes. Are we at the midpoint of a symbol/
              if( m_SamplesTillSymbolMidpoint == 0 )
              {
                  // Store the sample.
                  out[currentOutputSample++] = m_SignalValueUnity;
                  
                  // Set the next symbol midpoint
                  m_SamplesTillSymbolMidpoint = m_SamplesPerSymbol;
                  --m_SampleForNrOfSymbols;
                  
                  --samplesLeft;
                  break;
              }
          }
          
          // One sample done, one less sample to analyze.
          --samplesLeft;
          
          // If we are waiting for the midpoint to happen?
          if( m_SamplesTillSymbolMidpoint > 0 )
          {
              --m_SamplesTillSymbolMidpoint;
          }
          
      }
      
      // We just used a bunch of samples. Let the sheduler know.
      consume_each (ninput_items[0] - samplesLeft);

      // Tell runtime system how many output items we produced.
      return currentOutputSample;
    }

  } /* namespace keeloq */
} /* namespace gr */

