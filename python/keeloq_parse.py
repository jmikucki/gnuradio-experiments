#!/usr/bin/env python2
# -*- coding: utf-8 -*-
# 
# Copyright 2016 <+YOU OR YOUR COMPANY+>.
# 
# This is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
# 
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this software; see the file COPYING.  If not, write to
# the Free Software Foundation, Inc., 51 Franklin Street,
# Boston, MA 02110-1301, USA.
# 

import numpy
from gnuradio import gr
import sys

class keeloq_parse(gr.sync_block):
    """
    docstring for block keeloq_parse
    """
    
    counter = 0
    consumedBits = 0
    preambleNrOfOnes = 12
    consumedBit = [None]*(preambleNrOfOnes*2+1)
    startOfInterestingRegion = 0
    sizeOfInterstingRegion = 66*3+8
    interestingElementsLeft = 0
    interestingRegion = []
    
    
    def __init__(self):
        gr.sync_block.__init__(self,
            name="keeloq_parse",
            in_sig=[numpy.uint8],
            out_sig=None)

    def printKeeloq(self, packet, outputPacket):
        length = len(packet)
        state = ""
                
        for bit in packet:
            if( state == "" ):
                if( bit == 1):
                    state = "1"
                continue
            if( state == "1"):
                if( bit == 1):
                    state = "11"
                    continue
                if( bit == 0):
                    state = "10"
                    continue
            if( state == "11"):
                if( bit == 0):
                    outputPacket.append(1)
                    state = ""
                    continue
                if( bit == "1"):
                    state = "11"
                    continue
            if( state == "10"):
                if( bit == 0):
                    outputPacket.append(0)
                    state = ""
                    continue
                if( bit == 1):
                    state = "1"
                    continue

    def printKeeloqList(self, packetList):
        count = 0
        for bit in packetList:
            
            if(count == 32):
                sys.stdout.write(" ")
            if(count == 32 + 28):
                sys.stdout.write(" ")
            if( count == 32 + 28 + 4):
                sys.stdout.write(" ")
            
            count += 1
            
            if( bit == 0):
                sys.stdout.write("1")
            elif(bit == 1):
                sys.stdout.write("0")
            else:
                sys.stdout.write("X")
        
        print(" ")
            
            
    def work(self, input_items, output_items):
        in0 = input_items[0]
        
        if(self.consumedBits == self.preambleNrOfOnes*2):
            currentExpectedBit = 1
            found = True
            for i in range(self.preambleNrOfOnes*2-1):
                if(self.consumedBit[i] != currentExpectedBit):
                    found = False
                if(currentExpectedBit == 1):
                    currentExpectedBit = 0
                else:
                    currentExpectedBit = 1
                    
            if( found == True):
                #print "Found possible preamble at:", self.counter-3
                self.consumedBits = 0
                self.interestingElementsLeft = self.sizeOfInterstingRegion
                del self.interestingRegion[:]
            
            for i in range(self.preambleNrOfOnes*2):
                self.consumedBit[i] = self.consumedBit[i+1]            
            self.consumedBit[self.preambleNrOfOnes*2] = in0[0]
        else:
            self.consumedBit[self.consumedBits] = in0[0]
            self.consumedBits += 1
            
        self.counter += 1
        if(self.interestingElementsLeft > 0):
            self.interestingRegion.append(in0[0])
            
        if(self.interestingElementsLeft == 1):
            outputPacket = []
            self.printKeeloq(self.interestingRegion, outputPacket)
            #print(self.interestingRegion)
            self.printKeeloqList(outputPacket)
            del self.interestingRegion[:]
        self.interestingElementsLeft -= 1
        
        return 1
    
    
        
        
                    
    

